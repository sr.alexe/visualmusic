/* The Audio City Project aims to create a visually 
 * pleasing representation of any audio using the processing programming language. 
 * It uses the minim library to open and control the audio, 
 * which also provides functions to extract the volume of each different frequency band, 
 * this is the basic block to produce the visualization. 
 *
 * @author Alex Sáenz Rojas
 * @version 0.0.1
 * @since 2020-06-20
 */

import ddf.minim.analysis.*;
import ddf.minim.*;
import java.lang.Math; 

//Declaration of all used global variables
Minim       minim;
AudioPlayer player;
FFT         fft;
PImage sprite;  
ArrayList<ArrayList<Float>> matrix = new ArrayList<ArrayList<Float>>();
ArrayList<star> sky = new ArrayList<star>();
int historySize=100;
int R=33;
int G=45;
int B=74;

/*
* This method initializes the variables needed for the project. 
 *  It creates the sound player and loads the song.
 *  An array of bars are irizalixed with the first values of the fourier 
 * analysis of the start of the song. 
 * @params void
 * @return void
 */
void setup()
{
  size(1920, 400, P3D);//Configures the size of the window
  minim = new Minim(this); //initializes the audio player
  player = minim.loadFile("../../data/song2.mp3", 2048);
  player.loop();
  sprite = loadImage("../../data/sprite.png");

  fft = new FFT( player.bufferSize(), player.sampleRate() );
  for (int i=0; i<fft.specSize()/2; i++) {
    if (i%4==0) {
      matrix.add(new ArrayList<Float>());
      sky.add(new star());
    }
  }
}

/*
*This method is the loop that draws the images in the window
 *@params void
 *@return void
 */
void draw()
{
  float night=1;
  PShape stars = createShape(GROUP);

  /*CAlculates the RGB for the backgroud color depending on the time if the song*/
  if (player.position()<player.length()/4) {
    R=(int)map(player.position(), 0, player.length()/4, 0, 33);
    G=(int)map(player.position(), 0, player.length()/4, 0, 45);
    B=(int)map(player.position(), 0, player.length()/4, 0, 74);
    night=1;
  } else if (player.position()>player.length()/4&&player.position()<player.length()/2) {
    R=(int)map(player.position(), player.length()/4, player.length()/2, 33, 158);
    G=(int)map(player.position(), player.length()/4, player.length()/2, 45, 206);
    B=(int)map(player.position(), player.length()/4, player.length()/2, 74, 239);
    night=map(player.position(), player.length()/4, player.length()/3, 1, 0);
  } else if (player.position()>player.length()/2&&player.position()<(player.length()*3)/4) {
    R=(int)map(player.position(), player.length()/2, player.length()*0.75, 158, 86);
    G=(int)map(player.position(), player.length()/2, player.length()*0.75, 206, 98);
    B=(int)map(player.position(), player.length()/2, player.length()*0.75, 239, 172);
    night=map(player.position(), player.length()*0.66, player.length()*0.75, 0, 0.7);
  } else if (player.position()>(player.length()*0.75)&&player.position()<(player.length())) {
    R=(int)map(player.position(), player.length()*0.75, player.length(), 86, 0);
    G=(int)map(player.position(), player.length()*0.75, player.length(), 98, 0);
    B=(int)map(player.position(), player.length()*0.75, player.length(), 172, 0);
    night=1;
  }

  println(night);
  background(R, G, B); //Changes the background color

  fft.forward( player.mix ); // calculates the fast fourier transformation

  for (int j = 0; j <matrix.size(); j++)
  {      
    matrix.get(j).add(fft.getBand(j));

    //Creates the stars in the sky 
    if (night<0) {
      night=0;
    }
    float shine = (1+log(fft.getBand(j))*5)*night;

    //Creates the stars in the sky
    PShape star = createShape();
    star.beginShape(QUAD);
    star.texture(sprite);
    star.noStroke();
    star.translate(-width, -height, -500);
    star.vertex(sky.get(j).posX-shine, sky.get(j).posY-shine, 0, 0);
    star.vertex(sky.get(j).posX+shine, sky.get(j).posY-shine, sprite.width, 0);
    star.vertex(sky.get(j).posX+shine, sky.get(j).posY+shine, sprite.width, sprite.height);
    star.vertex(sky.get(j).posX-shine, sky.get(j).posY+shine, 0, sprite.height);
    star.endShape();
    stars.addChild(star);
    sky.get(j).move();

    if (matrix.get(j).size()>=historySize) {
      matrix.get(j).remove(0);
    }
  }
  shape (stars);


  // Creates multiple bars with diferent Zs to create a depth feeling 
  for (int i=0; i<matrix.size(); i++) {
    for (int j=0; j<matrix.get(i).size(); j++) {
      pushMatrix();
      fill(255);
      translate(0, 0, -j*5);
      pushMatrix();
      fill(255/log(i+1));
      translate(((((width+100)/2)/matrix.size())*i)+100, 400);
      rect( 0, 0, ((width/2)/matrix.size()), - matrix.get(i).get(j));
      popMatrix();
      pushMatrix();
      translate((width)-(((width/2)/matrix.size())*i)-100, 400);
      rect(0, 0, ((width/2)/matrix.size()), - matrix.get(i).get(j));
      popMatrix();      
      popMatrix();
    }
  }
}
//Class to create the stars in the sky
public class star {
  float posX; 
  float posY;

  star() {
    posX = random(0, width*4);
    posY = random(0, height*2);
  } 
  public void move() {
    posX = posX +random(-0.1, 1);
    posY = posY+ random(-0.1, 0.3);
    if ( posX >width*4) {
      posX = random(0, width*2);
    }
    if ( posY <-height*2) {
      posY = random(0, height*2);
    }
  }
}
