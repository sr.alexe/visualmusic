/* The Coronal Mass Ejection Project aims to create a visually 
* pleasing representation of any audio using the processing programming language. 
* It uses the minim library to open and control the audio, 
* which also provides functions to extract the volume of each different frequency band, 
* this is the basic block to produce the visualization. 
*
* @author Alex Sáenz Rojas
* @version 0.0.1
* @since 2020-06-20
*/
import ddf.minim.analysis.*;
import ddf.minim.*;
import java.lang.Math; 

//Declaration of all used global variables
PShape barsShape;
Minim       minim;
AudioPlayer player;
FFT         fft;
ArrayList<Float> bars = new ArrayList<Float>();

/*
* This method initializes the variables needed for the project. 
*  It creates the sound player and loads the song.
*  An array of bars are irizalixed with the first values of the fourier 
* analysis of the start of the song. 
* @params void
* @return void
*/

void setup() {
  size(900, 900, P3D); //Configures the size of the window
  minim = new Minim(this); //initializes the audio player
  player = minim.loadFile("song.mp3", 2048); // opens the song 
  player.loop(); //Sets the song on loop
  fft = new FFT( player.bufferSize(), player.sampleRate() ); // Configures the fourier transformation
  hint(DISABLE_DEPTH_MASK); //Needed to create 3D figures and avoid aliasing
  for (int i =0; i<fft.specSize()/10; i++) {
    bars.add(10.0);
  }
} 

/*
*This method is the loop that draws the images in the window
*@params void
*@return void
*/
void draw () {
  fill(0, 0, 0, 10);// alpha is used to create the vanishing effect  
  rect(0, 0, width, height); // create a black rectangle to paint the background
  fft.forward( player.mix );// loads the song in the fast fourier transformation
  barsShape = createShape(PShape.GROUP);
  float b=0;
  float mb=0;
  float mt=0;
  float t=0;
  
  // Calculate the variables of the central sphere
  for (int i =0; i<fft.specSize()/10; i++) {
    bars.set(i, (fft.getBand(i*10)+10) );
    if (i<25) {
      b=b+(fft.getBand(i*10));
    } else if ((i>25)&&(i<50)) {
      mb=mb+(fft.getBand(i*10));
    } else if ((i>50)&&(i<75)) {
      mt=mt+(fft.getBand(i*10));
    } else if ( (i>75)&&(i<100)) {
      t=t+(fft.getBand(i*10));
    }
  }
  // calculates the final size of the sphere
  int size =(int)((b/25)*0.1+(mb/25)*0.5+(mt/25)+(t/25)*2);
  
  
  pushMatrix();
  translate(width/2, height/2); // centers the sphere in the window  
  
  int R= 255;
  int G=(int)map (size, 0, 5, 255, 0);
  int B=(int)map (size, 0, 2, 255, 0); 
  stroke(R, G, B); //Changes colors of the sphere


  rotateY(frameCount*0.001);

  sphere(size*75); // Draws the sphere on the window
  popMatrix();
  
  translate(width/2, height/2); //centers the matrix 
  rotate(frameCount*0.001); 
  
  //Creates 100 bars to display the volume of each frequency band
  // by drawing a QUAD with the vertexes and the depth given by the volume
  for (int i =0; i<bars.size(); i++) {
    smooth();
    circle(0, 0, 1);
    pushMatrix();
    PShape bar = createShape();
    bar.rotate(radians(45), 0, 0, 1);
    bar.translate(100, 100, 0); 
    bar.rotate(((2*PI)/(bars.size()))*i, 0, 0, 1);  
    bar.beginShape(QUAD);  
    bar.fill((255), 255, 0);
    bar.noStroke();
    bar.vertex(-10, -10, -1000+(bars.get(i)*100));
    bar.vertex(10, -10, 100);
    bar.vertex(10, 10, 100);
    bar.vertex(-10, 10, -1000+(bars.get(i)*100));
    bar.endShape();
    barsShape.addChild(bar);
    popMatrix();
  }
  shape(barsShape);
}
