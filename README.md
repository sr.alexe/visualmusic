# VisualMusic!

In the Visualization1.pde and Visualization2.pde files there is an attempt to implement a visualization of audio files.  
This method consist of using  processing minim library and OpenGL to create visualizations based on the fast fourier transformation (fft)

This software is cross platform  since processing is based on Java.

The motivation is to create a visually  attractive  representations for blind people or public in general, based on music

---

# Files
|FILE|LANGUAGE|DESCRIPTION|
|-|-|-|
|VisualMusic1.pde|`Processing`|This contains a Audio City project |
|VisualMusic2.pde|`Processing`|Coronal Mass Ejection Project |

|song.mp3|`Audio File`|Song from Shell In The Pit THEY own the song |
|sprite.png|`Texture`|Texture for the stars|


---

## Requisites and Dependencies

To run VisualMusic the following software requisites must be met. 

|PROGRAM|VERSION|
|-|-|
|`Processing`|*3.5.x*|
|`Java`|*8  or newer*|

For Processing 3.5.x the following libraries must be installed.

|LIBRARY| VERSION|
|-|-|
|`minim`|*2.2.2^*|

---

## How to Run
1. Install Java
2. Install Processing 
3. Open the project click the "Play" button
4. Be sure of the path to the song you want to play



---
## Built With

* [Processing](https://processing.org/) - Programming language
* [GIMP](https://www.gimp.org/) - Image Editor

---
## Authors

* **Alex Saenz Rojas** - *Initial work* - [sr.alexe](https://gitlab.com/sr.alexe/)

---

## License

This project is licensed under the GPL License - see the [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) file for details


---

## Acknowledgments

* Thanks to my mom and GF fo all the coffee and patience

---
